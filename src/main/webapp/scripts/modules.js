'use strict';
angular.module('underscore', []).factory('_', function() {
  return window._; // assumes underscore has already been loaded on the page
});
angular.module('jquery', []).factory('$', function() {
  return window.jQuery; // assumes jQuery has already been loaded on the page
});
angular.module('moment', []).factory('moment', function() {
  return window.moment; // assumes moment has already been loaded on the page
});
angular.module('liferay', []).factory('Liferay', function() {
  return window.Liferay;
});
