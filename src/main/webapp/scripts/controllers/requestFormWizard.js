'use strict';
angular.module('xrasApp').controller('RequestFormWizardCtrl',
  function($scope, $routeParams, $location, $q, $timeout, $anchorScroll, WizardHandler, _, Api, Notify, RequestUtil) {

    $scope.request = {};

    // load all the things!
    $scope.loading = true;
    var deferred = {
      fos: $q.defer(),
      request: $q.defer()
    };

    // after everything is loaded, emit ready event
    var promises = _.map(deferred, function(d) { return d.promise; });
    $q.all(promises).then(function() {
      $scope.$broadcast('xras:requestReady', $scope.request);
      $scope.loading = false;
    });

    $scope.wizardStep = '';

    // Types
    $scope.types = {};

    // Fields of Science
    Api.Fos.getList().then(function(list) {
      $scope.fields = list;
      deferred.fos.resolve(list);
    });

    Api.Requests.one($routeParams.requestId).get()
    // check that request is editable
    .then(function(request) {
      if (request.requestStatus === 'Incomplete') {
        // pass to next thenable
        return request;
      } else {
        var confirmEdit = $q.defer();
        if (request.requestStatus === 'Submitted') {
          // prompt to withdraw submission
          if (window.confirm('Your current submission can be revised but please save and submit after you make the necessary revisions, otherwise the request will be incomplete.\n\nClick "OK" to continue.')) {
            // withdraw then pass to next thenable
            Api.Actions.withdraw(request, RequestUtil.getOriginalAction(request)).then(function() {
              confirmEdit.resolve(request);
            }, function(error) {
              confirmEdit.reject(error);
            });
          } else {
            // reject
            confirmEdit.reject(undefined);
          }
        } else {
          // disallow edits altogether
          confirmEdit.reject('This request is not editable.');
        }
        return confirmEdit.promise;
      }
    }, function(error) {
      if (error.status === 404) {
        Notify.message({
          body: 'The Request was not found!',
          type: 'danger'
        });
      } else {
        Notify.message({
          body: 'There was and error loading the Request. Please try again.',
          type: 'danger'
        });
      }
      $location.path('/');
    })
    // setup scope
    .then(function(request) {
      $scope.request = request;
      $scope.action = RequestUtil.getOriginalAction($scope.request);
      Api.Opportunities.one(request.opportunityId).get().then(function(opp) {
        $scope.opportunity = opp;
        var actionTypeId = -1;
        Api.ActionType.one($scope.action.actionType).get().then(
            function(id) {
                actionTypeId = id;
                Api.RequiredFields.one($scope.opportunity.allocationTypeInfo.allocationTypeId).one(actionTypeId).get().then(
                    function(required) {
                        $scope.requiredFields = required.requiredFields;
                    });
                });

        deferred.request.resolve(request);
      });
    }, function(reason) {
      if (reason) {
        Notify.message({
          body: reason,
          type: 'danger'
        });
      }
      window.history.back();
    });

    // events

    $scope.$on('xras:requestChanged', function(e, data) {
      if (data) {
        $scope.requestForm.$setDirty();
      }
    });

    $scope.validation = {
      status: '',
      errors: []
    };

    var doValidate = function doValidate() {
      $scope.validation.status = 'validating';
      $scope.validation.errors = [];
      Api.Actions.validate($scope.request, $scope.action).then(
        function(result) {
          if (result.validation === 'failed') {
            $scope.validation.status = 'invalid';
            $scope.validation.errors = result.errors;
          } else {
            $scope.validation.status = 'valid';
            $scope.validation.errors = [];
          }
        }
      );
    };

    $scope.$on('wizard:stepChanged', function(e, data) {
      Notify.clear('all');
      $anchorScroll();

      if (data.step.wzTitle === 'Submit') {
        $scope.validation.status = 'validating';
      }

      if ($scope.requestForm && $scope.requestForm.$dirty) {
        $scope.savingChanges = true;
        $scope.request.save().then(
          function(req) {
            $scope.request = req;
            $scope.action = RequestUtil.getOriginalAction($scope.request);
            $scope.$broadcast('xras:requestReady', $scope.request);
            $scope.requestForm.$setPristine();
          },
          function(reason) {
            if (reason.status === 400) {
              Notify.message({
                body: reason.statusText + ': ' + reason.data,
                type: 'danger'
              });
            } else {
              Notify.message({
                body: 'An unexpected error occurred while saving the Request. Please try again. If this problem persists, please contact the Help Desk.',
                type: 'danger'
              });
            }
          }
        ).then(function() {
          $scope.savingChanges = false;
          if (data.step.wzTitle === 'Submit') {
            doValidate();
          }
        });
      } else if (data.step.wzTitle === 'Submit') {
        doValidate();
      }
    });

    $scope.saveAndStep = function(forward) {
      Notify.clear('all');
      if ($scope.requestForm.$dirty) {
        $scope.savingChanges = true;
        $scope.request.save().then(function(req) {
          $scope.request = req;
          $scope.action = RequestUtil.getOriginalAction($scope.request);
          $scope.$broadcast('xras:requestReady', $scope.request);
          $scope.savingChanges = false;
          $scope.requestForm.$setPristine();
          if (forward) {
            WizardHandler.wizard().next();
          } else {
            WizardHandler.wizard().previous();
          }
        }, function(reason) {
          if (reason.status === 400) {
            Notify.message({
              body: reason.statusText + ': ' + reason.data,
              type: 'danger'
            });
          } else {
            Notify.message({
              body: 'An unexpected error occurred while saving the Request. Please try again. If this problem persists, please contact the Help Desk.',
              type: 'danger'
            });
          }
          $scope.savingChanges = false;
          $anchorScroll();
        });
      } else {
        if (forward) {
          WizardHandler.wizard().next();
        } else {
          WizardHandler.wizard().previous();
        }
      }
    };

    $scope.advanced = function() {
      Notify.clear('all');
      if ($scope.requestForm.$dirty) {
        // save changes first!
        $scope.savingChanges = true;
        $scope.request.save().then(function() {
          $location.path($location.path() + '/advanced');
        }, function(reason) {
          if (reason.status === 400) {
            Notify.message({
              body: reason.statusText + ': ' + reason.data,
              type: 'danger'
            });
          } else {
            Notify.message({
              body: 'An unexpected error occurred while saving the Request. Please try again. If this problem persists, please contact the Help Desk.',
              type: 'danger'
            });
          }
          $scope.savingChanges = false;
          $anchorScroll();
        });
      } else {
        $location.path($location.path() + '/advanced');
      }
    };

    $scope.submitRequest = function() {
      Notify.clear('all');
      $scope.savingChanges = true;
      // validate once more to be sure!
      var action = RequestUtil.getOriginalAction($scope.request);
      Api.Actions.submit($scope.request, action).then(
        function() {
          Notify.message({
            body: 'Your request has been submitted!',
            type: 'success'
          });
          $scope.savingChanges = false;
          $location.path('/requests/' + $scope.request.requestId);
        },
        function() {
          Notify.message({
            body: 'An unexpected error occurred! If this problem persists, please contact the Help Desk.',
            type: 'danger'
          });
          $scope.savingChanges = false;
        }
      );
    };

    $scope.saveForLater = function() {
      Notify.clear('all');
      Notify.message({
        body: 'Your Request has been saved! Please note, you still need to submit your request before it can be considered for review.',
        type: 'success'
      });
      $location.path('/requests/' + $scope.request.requestId);
    };
  }
);
