'use strict';
angular.module('xrasApp').controller('RequestNewActionCtrl', function($scope, $routeParams, $location, $q, _, Api, Notify) {

  $scope.model = {};
  $scope.actionType = $routeParams.actionType;

  $scope.createAction = function() {
    $scope.model.saving = true;
    var newAction = { actionType: $scope.actionType };
    Api.Actions.save($scope.request, newAction)
    .then(function(request) {
      newAction = _.first(request.actions);
      $location.path('/requests/' + request.requestId + '/actions/' + newAction.actionId);
    }, function(error) {
      $scope.model.saving = false;
      Notify.message({
        body: 'There was an error creating your ' + $scope.actionType + ' action! Please try again. If this problem persists, please contact the Help Desk. Error details: ' + error,
        type: 'danger'
      });
    });
  };

  /**
   * Async tasks:
   * 0. Requirements
   * 1. Request
   * 2. Opportunity
   */
  var tasks = [$q.defer(),$q.defer(),$q.defer()];

  $scope.loaded = false;

  Api.Requests.one('requirements').get().then(function(requirements) {
    $scope.requirements = requirements;
    tasks[0].resolve(requirements); // Task 0.
  });

  Api.Requests.one($routeParams.requestId).get().then(function(request) {
    $scope.request = request;

    Api.Opportunities.one(request.opportunityId).get().then(function(opportunity) {
      $scope.opportunity = opportunity;
      tasks[2].resolve(opportunity); // Task 2.
    });

    tasks[1].resolve(request); // Task 1.
  });

  $q.all(_.pluck(tasks, 'promise')).then(function() {
    $scope.loaded = true;
  });

});
