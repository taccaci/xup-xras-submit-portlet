'use strict';
angular.module('xrasApp').controller('RequestFormDocumentsCtrl', function($scope, $timeout, $q, $upload, $, _, AppUtil, Api, Notify) {

  $scope.$on('xras:requestReady', function() {
    Api.Actions.requiredDocumentsStatus($scope.request, $scope.action).getList().then(function(requiredDocuments) {
        $scope.documentRequirements = requiredDocuments;
    });


    Api.DocTypes.getList().then(function(list) {
        $scope.docTypesIndex = _.indexBy(list, 'documentType');

        //filter by active
        var activeTypes = _.filter(list, function(dt) {
            return dt.isActive;

        });

        // Filter all document types that have the correct action type and allocation type id
        var filtered = _.filter(activeTypes, function(documentType) {
                var allocationActionTypes = documentType.allocationActionTypes;
                var hasActionType = _.some(allocationActionTypes, function(allocationActionType) {
                        var hasTypeId = allocationActionType.allocationTypeId === $scope.opportunity.allocationTypeInfo.allocationTypeId;
                        var hasActionType = _.some(allocationActionType.actionTypes, function(actionType) {
                                return actionType.actionType === $scope.action.actionType;
                            });
                        return hasTypeId && hasActionType;
                    });
                return hasActionType;
            });

        $scope.docTypes = filtered;
    });

  });

    $scope.$on('xras:requestChanged', function() {
      Api.Actions.requiredDocumentsStatus($scope.request, $scope.action).getList().then(function(requiredDocuments) {
          $scope.documentRequirements = requiredDocuments;
      });

            // unsure if this needs to be done here as well?
          Api.DocTypes.getList().then(function(list) {
              $scope.docTypesIndex = _.indexBy(list, 'documentType');

              //filter by active
              var activeTypes = _.filter(list, function(dt) {
                  return dt.isActive;

              });

              // Filter all document types that have the correct action type and allocation type id
              var filtered = _.filter(activeTypes, function(documentType) {
                      var allocationActionTypes = documentType.allocationActionTypes;
                      var hasActionType = _.some(allocationActionTypes, function(allocationActionType) {
                              var hasTypeId = allocationActionType.allocationTypeId === $scope.opportunity.allocationTypeInfo.allocationTypeId;
                              var hasActionType = _.some(allocationActionType.actionTypes, function(actionType) {
                                      return actionType.actionType === $scope.action.actionType;
                                  });
                              return hasTypeId && hasActionType;
                          });
                      return hasActionType;
                  });

              $scope.docTypes = filtered;
          });

    });

  $scope.editing = null;

  $scope.downloadDocumentUrl = function(doc) {
    return Api.Documents.downloadUrl($scope.request, $scope.action, doc);
  };

  $scope.removeDocument = function($index, doc) {
    if (window.confirm('Are you sure you want to delete the document "' + doc.title +'"?')) {
      Api.Documents.remove($scope.request, $scope.action, doc).then(
        function() {
          $scope.action.documents.splice($index,1);
          Notify.message({
            body: 'The document has been removed from this request.',
            type: 'success'
          });
        },
        function() {
          Notify.message({
            body: 'There was an error deleting this document. If this problem persists, please contact the Help Desk.',
            type: 'danger'
          });
        }
      );
    }
  };

  $scope.addDocument = function() {
    $scope.action.documents = $scope.action.documents || [];
    $scope.editing = {
      $index: $scope.action.documents.length
    };
    $timeout(function() {
      $('html,body').scrollTop($('.block-ui-modal-dialog:visible').offset().top - 50);
    });
  };

  $scope.onFileSelect = function($files) {
    $scope.$files = $files;
  };

  $scope.uploadDocument = function() {
    Notify.clear('docs');

    if ($scope.$files && $scope.$files.length > 0) {
      $scope.uploadInProgress = true;
      $scope.upload = $upload.upload({
        url: Api.Documents.uploadUrl($scope.request, $scope.action),
        method: 'POST',
        data: $scope.editing,
        file: $scope.$files[0],
        fileFormDataName: 'upload'
      })
      .success(function(data) {
        $scope.uploadInProgress = false;
        $scope.action.documents = _.findWhere(data.actions, { actionId: $scope.action.actionId }).documents;
        $scope.editing = null;
        Notify.message({
          body: 'Your document was uploaded successfully!',
          type: 'success'
        });
      })
      .error(function(err) {
        $scope.uploadInProgress = false;
        Notify.message({
          key: 'docs',
          body: err,
          type: 'danger'
        });
      });
    } else {
      Notify.message({
        key: 'docs',
        body: 'You haven\'t selected a file to upload!',
        type: 'danger'
      });
    }
  };

  $scope.cancelUpload = function() {
    Notify.clear('docs');
    $scope.editing = null;
  };

  $scope.convertBytes = AppUtil.convertBytes;

  $scope.close = Notify.remove;

  $scope.$on('xras:notify', function() {
    $scope.messages = Notify.messages();
  });
});
