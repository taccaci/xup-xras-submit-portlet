'use strict';
angular.module('xrasApp').controller('RequestEditActionCtrl', function($scope, $routeParams, $location, $timeout, $q, moment, _, Api, Notify) {

  $scope.model = {};

  /**
   * Loading Tasks
   * 0. request
   * 1. opportunity
   * 2. current_dates (only for extensions, otherwise null task)
   */
  var tasks = [ $q.defer(), $q.defer(), $q.defer() ];

  $scope.loaded = false;

  Api.Requests.one($routeParams.requestId).get().then(function(request) {
    $scope.request = request;
    $scope.action = _.findWhere(request.actions, { actionId: parseInt( $routeParams.actionId, 10 ) });

    Api.Opportunities.one(request.opportunityId).get().then(function(opp) {
      $scope.opportunity = opp;
      tasks[1].resolve(opp); // Task 1.

    }, function(error) {
      tasks[1].reject(error); // Task 1. FAIL
    });

    // Resources excluded from Appeals and Extensions, and Final Reports
    $scope.requireResources = $scope.action.actionType !== 'Appeal' && $scope.action.actionType !== 'Extension' && $scope.action.actionType !== 'Final Report';
    if ($scope.action.actionType === 'Appeal') {
      // load details for action resources
      $scope.resourceIndex = {};
      _.each($scope.action.resources, function(resource) {
        Api.Resources.one(resource.resourceId).get().then(function(resp) {
          $scope.resourceIndex[resource.resourceId] = resp;
        });
      });
    }

    if ($scope.action.actionType === 'Extension') {
      $scope.action.allocationDates[0] = $scope.action.allocationDates[0] || {
        allocationDateType: 'Requested',
        endDate: ''
      };

      Api.Requests.one(request.requestId).one('dates').get().then(
        function(dates) {
          // this should never be needed, but prevents errors.
          dates.endDate = dates.endDate || moment().endOf('month').format('YYYY-MM-DD');

          // extension options are calculated by adding a day to roll the month
          // over, add months, then subtract a day to roll the month back. this
          // handles where the last day of the month varies between 30 and 31.
          dates.extensions = [{
            label: '3 Months',
            date: moment(dates.endDate).add('days', 1).add('months', 3).add('days', -1).format('YYYY-MM-DD')
          }, {
            label: '6 Months',
            date: moment(dates.endDate).add('days', 1).add('months', 6).add('days', -1).format('YYYY-MM-DD')
          }];
          $scope.dates = dates;
          tasks[2].resolve(dates); // Task 2.
        },
        function(error) {
          // TODO
          tasks[2].reject(error); // Task 2. FAIL
        }
      );
    } else {
      tasks[2].resolve(null);
    }

    switch ($scope.action.actionType) {
    case 'Transfer':
      $scope.resourceInfo = '<p>To request a transfer of SUs between resources, please check the <a target="_blank" href="https://www.xsede.org/su-converter">SU Converter</a> to calculate the correct numbers for your transfer request.</p><p><strong>Please indicate the resource(s) to transfer FROM using a NEGATIVE number and the resource(s) to transfer TO using a POSITIVE number.</strong></p>';
      break;
    case 'Advance':
      $scope.resourceInfo = '<p>You may request an advance of <b>up to 10%</b> of the original requested amount for each resource.</p>';
      break;
    case 'Supplement':
        // post the withdrawl here IF the supplement has been submitted already
        if ($scope.action.actionStatus === 'Submitted') {
            Api.Actions.withdraw($scope.request, $scope.action).then(function() {
                    Notify.message({
                          body: 'Your submission has been withdrawn, please re-submit once you have finished editing.',
                          type: 'danger'
                        });
            });
        }
        break;
    case 'Appeal':
    }

    tasks[0].resolve(request); // Task 0.
  }, function(error) {
    tasks[0].reject(error); // Task 0. FAIL
  });

  $q.all(_.pluck(tasks, 'promise')).then(function() {
    $scope.loaded = true;
    $timeout(function() {
      $scope.$broadcast('xras:requestReady', $scope.request);
    });
  });

  // events

  // This was commented out because it doesn't seem to be doing anything useful
  // and was causing an issue where supplements would display an error related to
  // grants because it was running a full save on load
/**
      $scope.$on('xras:requestChanged', function() {
          $scope.model.saving = false;
          $scope.request.save().then(
            function(req) {
              $scope.request = req;
              // $scope.action = RequestUtil.getOriginalAction($scope.request);
              $scope.$broadcast('xras:requestReady', $scope.request);
            },
            function(reason) {
              if (reason.status === 400) {
                Notify.message({
                  body: reason.statusText + ': ' + reason.data,
                  type: 'danger'
                });
              } else {
                Notify.message({
                  body: 'An unexpected error occurred while saving the Request. Please try again. If this problem persists, please contact the Help Desk.',
                  type: 'danger'
                });
              }
            }
          ).then(function() {
            $scope.model.saving = false;
          });
          });
          **/

  $scope.validateAndSubmit = function() {
    $scope.model.saving = true;
    Api.Actions.save($scope.request, $scope.action)
      .then(function() {
        return Api.Actions.validate($scope.request, $scope.action);
      })
      .then(function(result) {
        if (result.validation === 'failed') {
          _.each(result.errors, function(error) {
            Notify.message({
              body: error,
              type: 'danger'
            });
          });
          return $q.reject(result);
        } else {
          return Api.Actions.submit($scope.request, $scope.action);
        }
      })
      .then(function() {
        Notify.message({
          body: 'Your ' + $scope.action.actionType + ' request has been submitted!',
          type: 'success'
        });
        $location.path('/requests/' + $scope.request.requestId);
      })
      .then(null, function(error) {
        $scope.model.saving = false;
        Notify.message({
          body: 'There was an error saving your ' + $scope.action.actionType + ' request. Please try again. If this problem persists, please contact the Help Desk. Error details: ' + error.statusText,
          type: 'danger'
        });
      });
  };

  $scope.saveAction = function() {
    $scope.model.saving = true;
    Api.Actions.save($scope.request, $scope.action)
      .then(function() {
        Notify.message({
          body: 'Your ' + $scope.action.actionType + ' request has been saved! You will need to return to submit it before it will be reviewed.',
          type: 'success'
        });
        $location.path('/requests/' + $scope.request.requestId);
      }, function(error) {
        $scope.model.saving = false;
        Notify.message({
          body: 'There was an error saving your ' + $scope.action.actionType + ' request. Please try again. If this problem persists, please contact the Help Desk. Error details: ' + error.statusText,
          type: 'danger'
        });
      });

  };

});
