'use strict';
angular.module('xrasApp')
  .controller('BreadcrumbsCtrl', function ($scope, $routeParams, breadcrumbs) {
    breadcrumbs.options = {
      'New Request Action': 'New ' + $routeParams.actionType
    };
    $scope.breadcrumbs = breadcrumbs;
  });
