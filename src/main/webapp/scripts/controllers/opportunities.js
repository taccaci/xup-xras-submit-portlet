'use strict';
angular.module('xrasApp').controller('OpportunityCtrl', function ($scope, $routeParams, $location, Api, Notify, _) {
  Api.Opportunities.one($routeParams.opportunityId).get().then(function(opp) {
    $scope.opportunity = opp;
    $scope.opportunity.hasRequirements = true;
    $scope.opportunity.resources = _.filter(opp.resources, function(resource) {
      return _.contains(opp.rules.resourceIdsAvailableForNewRequest, resource.resourceId);
    });
  }, function(error) {
    if (error.status === 404) {
      Notify.message({
        body: 'The Opportunity was not found!',
        type: 'danger'
      });
    } else {
      Notify.message({
        body: 'There was and error loading the Opportunity. Please try again.',
        type: 'danger'
      });
    }
    $location.path('/');
  }).then(function() {
    Api.Requests.one().one('requirements').get().then(function(resp) {
           var allocationType = $scope.opportunity.allocationType.toLowerCase().replace(/\s/g, '_');

           if (allocationType in resp) {
            var requirements = resp[allocationType];
            var newKeys = _.keys(requirements['new']);
            var renewalKeys = _.keys(requirements.renewal);
            $scope.opportunity.hasRequirements = newKeys || renewalKeys;
          } else {
            $scope.opportunity.hasRequirements = false;
          }

        });

  });

  $scope.showResourceDetails = {};
  $scope.toggleResourceDetails = function(resourceId) {
    $scope.showResourceDetails[resourceId] = ! $scope.showResourceDetails[resourceId];
  };
});
