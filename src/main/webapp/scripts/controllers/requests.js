'use strict';
angular.module('xrasApp')

  // Request Display
  .controller('RequestCtrl', function (_, $scope, $routeParams, $q, $location, Notify, Api, AppUtil, RequestUtil) {

    $scope.loading = true;

    // load all the things!
    var deferred = {
      fos: $q.defer(),
      fa: $q.defer(),
      request: $q.defer(),
      opportunity: $q.defer(),
      docTypes: $q.defer()
    };

    Api.DocTypes.getList().then(function(list) {
      $scope.docTypesIndex = _.indexBy(list, 'documentType');
      deferred.docTypes.resolve(list);
    });

    Api.Fos.one().get({map:true}).then(function(fos) {
      $scope.fields = fos;
      deferred.fos.resolve(fos);
    });

    Api.FundingAgencies.one().get({map:true}).then(function(map) {
      $scope.fundingAgencies = map;
      deferred.fa.resolve(map);
    });

    Api.Requests.one($routeParams.requestId).get().then(
      function(req) {
        req.rules = req.rules || {};

        $scope.request = req;
        $scope.allowedOps = {};

        _.each(req.rules.allowedOperations, function(op) {
          $scope.allowedOps['can'+op] = true;
        });


        $scope.existingActions = _.indexBy(req.rules.existingActions, 'actionId');
        _.each($scope.existingActions, function(existingAction) {
          if (existingAction.actionType !== 'New' && existingAction.actionType !== 'Renewal') {
            if (_.contains(existingAction.allowedOperations, 'Edit')) {
              existingAction.canEdit = true;
            }
            if (_.contains(existingAction.allowedOperations, 'Delete')) {
              existingAction.canDelete = true;
            }
          }
        });

        // get resource info
        var selectedResources = $scope.selectedResources = {};
        _.each(req.actions, function(action) {
          var ram = {};
          _.each(action.resourceAttributes, function(attr) {
            ram[attr.resourceId] = ram[attr.resourceId] || {};
            ram[attr.resourceId][attr.attributeId] = attr;
          });
          action.resourceAttributes = ram;

          action.resourceIndex = _.groupBy(action.resources, 'resourceId');

          selectedResources[action.actionId] = {};

          _.each(_.keys(action.resourceIndex), function(resourceId) {
            Api.Resources.one(resourceId).get().then(function(res) {
              selectedResources[action.actionId][res.resourceId] = res;
            });
          });
        });

        Api.Opportunities.one(req.opportunityId).get().then(function(opp) {
          $scope.opportunity = opp;

          _.each(req.actions, function(action) {
            action.opportunityAttributes = _.indexBy(action.opportunityAttributes, 'attributeId');
          });

          deferred.opportunity.resolve(opp);
        });
        deferred.request.resolve(req);
      },
      function(error) {
        if (error.status === 404) {
          Notify.message({
            body: 'The Request was not found!',
            type: 'danger'
          });
        } else {
          Notify.message({
            body: 'There was an error loading the Request. Please try again.',
            type: 'danger'
          });
        }
        $location.path('/');
      }
    );

    $q.all(_.map(deferred, function(d) { return d.promise; })).then(function() {
      $scope.loading = false;
    });

    $scope.validateRequest = function($event, request) {
      if ($event) {
        $event.preventDefault();
      }

      Notify.clear('all');

      var deferred = $q.defer();
      var action = RequestUtil.getOriginalAction(request);
      Api.Actions.validate(request, action).then(
        function(result) {
          if (result.validation === 'failed') {
            _.each(result.errors, function(error) {
              Notify.message({
                body: error,
                type: 'danger'
              });
            });
            deferred.reject(result);
          } else {
            if ($event) {
              Notify.message({
                body: 'Your request validated successfully! You are ready to submit your request for review.',
                type: 'success'
              });
            }
            deferred.resolve(result);
          }
        },
        function(error) {
          Notify.message({
            body: 'An unexpected error occurred. We were unable to validate your request. If this problem continues, please contact the Help Desk.',
            type: 'danger'
          });
          deferred.reject(error);
        }
      );

      return deferred.promise;
    };

    $scope.finalizeRequest = function($event, request) {
      $event.preventDefault();
      $scope.validateRequest(null, request).then(function() {
        var action = RequestUtil.getOriginalAction(request);
        Api.Actions.submit(request, action).then(
          function() {
            Notify.message({
              body: 'Your request has been submitted!',
              type: 'success',
            });
          },
          function() {
            Notify.message({
              body: 'An unexpected error occurred. We were unable to submit your request. If this problem continues, please contact the Help Desk.',
              type: 'danger',
            });
          }
        );
      });
    };

    $scope.deleteRequest = function(request) {
      if (window.confirm('Are you sure you want to delete this request?')) {
        Api.Requests.one(request.requestId).remove().then(
          function() {
            $location.path('/');
            Notify.message({
              body: 'The request was deleted!',
              type: 'success'
            });
          },
          function() {
            Notify.message({
              body: 'There was an error deleting this request. Please try again.',
              type: 'danger'
            });
          }
        );
      }
    };

    $scope.deleteAction = function($event, action) {
      if (window.confirm('Are you sure you want to delete this ' + action.actionType + '?')) {
        Api.Actions.remove($scope.request, action).then(
          function(request) {
            $scope.request = request;
          },
          function() {
            Notify.message({
              body: 'There was an error deleting this action. Please try again.',
              type: 'danger'
            });
          }
        );
      }
    };

    $scope.convertBytes = AppUtil.convertBytes;

    $scope.downloadDocumentUrl = Api.Documents.downloadUrl;

    $scope.requestRoleSort = RequestUtil.requestRoleSort;

  });
