'use strict';
angular.module('xrasApp').controller('RequestFormGrantsCtrl', function($scope, $timeout, $, _, Api, Notify) {

  $scope.$on('xras:requestReady', function() {
    // Get list of Funding Agencies
    Api.FundingAgencies.getList().then(function(list) {
      $scope.fundingAgencyList = list;
      $scope.fundingAgencyMap = _.indexBy(list, 'fundingAgencyId');

      //console.log($scope.fundingAgencyMap);
      // funding agency map:
      // { 1: NSF, 2: ACS/PRF, 3: USDA, 4: DOC, 5: DOD, 6: ED, 7: DOE, 8: DOI, 9: DOT, 10: EPA,
      // 11: Foundation, 12: Industry, 13: International, 14: NASA, 15: NIH, 16: Smithsonian,
      // 17: State (any state), 18: University, 19: Other }
      // Valid NIH agencies (departments in the NIH API):
      // USDA [ARS, FS, NIFA], DOD [CNRM, CCCRP, CDMRP, DVBIC], EPA, HHS [ACF, AHRQ, ALLCDC, FDA, NIDILRR, NIH], NASA, NSF, VA

    });
  });

  $scope.editing = null;
  $scope.awardError = false;

  $scope.datepickerOpts = {
    format: 'yyyy-mm-dd',
    autoclose: true,
    placeholder: 'YYYY-MM-DD'
  };

  // add grant
  $scope.add = function() {
    $scope.request.grants = $scope.request.grants || [];
    $scope.edit($scope.request.grants.length, {awardedUnits: 'Dollars'});
  };

    $scope.searchNSF = function() {
    // search calls the nsf's api endpoint. documentation here:
    // https://www.research.gov/common/webapi/awardapisearch-v1.htm#output-tags
        var grantId = $scope.editing.grantNumber.substring($scope.editing.grantNumber.length - 7);
        $.ajax({
            url: '/nsf/services/v1/awards/' + grantId  + '.json',
            data: 'printFields=id,title,agency,awardAgencyCode,poName,poEmail,pdPIName,awardee,startDate,expDate,fundsObligatedAmt,estimatedTotalAmt,publicationResearch',
            dataType: 'json'
        }).then(function(resp) {
            // since we're pulling by id there should only be 1 in this array
            if (resp.response.award.length <= 0) {
            $scope.$apply(function() {
                $scope.awardError = true;
                });
             } else {
             var award = resp.response.award[0];
            $scope.$apply(function() {
                $scope.awardError = false;
                $scope.editing.grantNumber = award.id;
                $scope.editing.title = award.title;
                $scope.editing.programOfficerName = award.poName;
                $scope.editing.programOfficerEmail = award.poEmail;
                $scope.editing.piName = award.pdPIName;

                // setting the Funding Agency drop down to NSF since we know it must be.
                // NSF is option with value 1 in the dropdown
                $scope.editing.fundingAgencyId = 1;
                // the api returns MM/DD/YYYY, we need it to be YYYY-MM-DD
                var start = award.startDate.split('/');
                var end = award.expDate.split('/');

                $scope.editing.beginDate = start[2] + '-' + start[0] + '-' + start[1];
                $scope.editing.endDate = end[2] + '-' + end[0] + '-' + end[1];

                $scope.editing.awardedAmount = award.fundsObligatedAmt;
            });
            }
        });
    };

       $scope.searchNIH = function() {
        // search calls the nih's api endpoint. documentation here:
        // https://api.federalreporter.nih.gov/
            var grantId = $scope.editing.grantNumber;
            $.ajax({
                url: '/nih/services/v1/Projects',
                data: 'projectNumber='+grantId,
                dataType: 'json'
            }).then(function(resp) {
                // making sure the return object has a projectNumber
                if (!resp.hasOwnProperty('projectNumber') ) {
                $scope.$apply(function() {
                    $scope.awardError = true;
                    });
                 } else {
                 var award = resp;
                $scope.$apply(function() {
                    $scope.awardError = false;
                    $scope.editing.grantNumber = award.projectNumber;
                    $scope.editing.title = award.title;
                    $scope.editing.piName = award.contactPi;

                    // the NIH API returns the department and agency of the grant. I'm using this to try and
                    // find the agency in the dropdown, though we have to use the department field here because
                    // agency is too fine-grained for our case.

                    // NIH is 15 in the dropdown, that's the default if we can't find it in the map
                          var agency = _.findWhere($scope.fundingAgencyMap, { fundingAgencyAbbr: award.department });

                          if (agency) {
                            $scope.editing.fundingAgencyId = agency.fundingAgencyId;

                          } else {
                          $scope.editing.fundingAgencyId = 15;
                          }


                    // the api returns YYYY-MM-DDT00:00:00, we need it to be YYYY-MM-DD
                    $scope.editing.beginDate = award.projectStartDate.split('T')[0];
                    $scope.editing.endDate = award.projectEndDate.split('T')[0];

                    //$scope.editing.beginDate = start[2] + '-' + start[0] + '-' + start[1];
                    //$scope.editing.endDate = end[2] + '-' + end[0] + '-' + end[1];

                    $scope.editing.awardedAmount = award.totalCostAmount;
                });
                }
            }).fail(function() {
                $scope.$apply(function() {
                                    $scope.awardError = true;
                                    });
            });
        };

  $scope.edit = function($index, grant) {
    $scope.editing = angular.copy(grant);
    $scope.editing.$index = $index;
    $timeout(function() {
      $('html,body').scrollTop($('.block-ui-modal-dialog:visible').offset().top - 50);
    });
  };

  $scope.submit = function() {
    Notify.clear('grants');
    $scope.saveProgress = true;
    Api.Grants.save($scope.request, $scope.editing).then(
      function(resp) {
        _.extend($scope.request.grants, resp.grants);
        $scope.editing = null;
        $scope.saveProgress = false;

        Notify.message({
          body: 'Your Grant information was saved and added to the Request.',
          type: 'success'
        });
      },
      function(error) {
        Notify.message({
          key: 'grants',
          body: error.data,
          type: 'danger'
        });
        $scope.saveProgress = false;
      }
    );
  };

  $scope.cancel = function() {
    Notify.clear('grants');
    $scope.editing = null;
  };

  // remove grant
  $scope.remove = function($index, grant) {
    if (window.confirm('Are you sure you want to the grant "' + grant.title +'"?')) {
      Api.Grants.remove($scope.request, grant).then(
        function(/*resp*/) {
          Notify.message({
            body: 'This Grant has been deleted',
            type: 'success'
          });
          $scope.request.grants.splice($index, 1);
          // $scope.$emit('xras:requestChanged', { key: 'grants' });
        },
        function(error) {
          Notify.message({
            body: 'There was an error deleting this Grant. ' + error + ' If this problem persists, please contact the Help Desk.',
            type: 'danger'
          });
        }
      );

    }
  };

  $scope.close = Notify.remove;

  $scope.$on('xras:notify', function() {
    $scope.messages = Notify.messages();
  });
});
