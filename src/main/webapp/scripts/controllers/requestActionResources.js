'use strict';
angular.module('xrasApp').controller('ActionResourcesCtrl', function($scope, $q, _, Api, RequestUtil, ResourceAttributeUtil) {
  var opportunity,
    action,
    originalAction,
    originalResources,
    resources,
    processSelection,
    resourceAttributesForm;

  opportunity = $scope.opportunity;
  action = $scope.action;
  resources = {};

  if (action.actionType === 'Advance') {
    // only use resources from original request
    originalAction = RequestUtil.getOriginalAction($scope.request);
    originalResources = _.pluck(originalAction.resources, 'resourceId');
    resources.options = _.filter(opportunity.resources, function(resource) {
      return _.contains(originalResources, resource.resourceId);
    });
    resources.index = _.indexBy(resources.options, 'resourceId');
  } else {
    // get resources from rules
    resources.index = _.findWhere($scope.request.rules.allowedActionsRes, {actionType: action.actionType}).availableResourceIds;
    resources.options = _.filter(opportunity.resources, function(resource) {
      return _.contains(resources.index, resource.resourceId);
    });
  }

  resources.selected = {};
  resources.removed = {};
  $scope.resources = resources;
  $scope.resourceAttributeMap = {};

  resourceAttributesForm = function() {
    $scope.resourceAttributeMap = ResourceAttributeUtil.map($scope.action, $scope.resources.selected);
  };

  $scope.selectResource = function(resource) {
    // safety first!
    resources.selected[resource.resourceId] = resources.selected[resource.resourceId] || {};
    resources.selected[resource.resourceId].selected = ! resources.selected[resource.resourceId].selected;
    processSelection(resource, resources.selected[resource.resourceId].selected).then(resourceAttributesForm);
  };

  processSelection = function (resource, select) {
    var deferred = $q.defer(), i;

    // safety first!
    resources.selected[resource.resourceId] = resources.selected[resource.resourceId] || {};
    resources.selected[resource.resourceId].selected = select;
    if (select) {
      // check if already selected
      i = _.map(action.resources, function(r) { return r.resourceId; }).indexOf(resource.resourceId);
      if (i === -1) {
        // add resource
        action.resources.push(resources.removed[resource.resourceId] || {
          resourceId: resource.resourceId,
          resourceName: resource.resourceName,
          type: 'Requested'
        });
      }

      if (resources.selected[resource.resourceId].resourceId) {
        deferred.resolve(resources.selected[resource.resourceId]);
      } else {
        // load rest of resource information
        Api.Resources.one(resource.resourceId).get().then(function(resp) {
          _.extend(resources.selected[resource.resourceId], resp);

          if (action.actionType === 'Advance') {
            // original request, for reference
            resources.selected[resource.resourceId].originalRequest = _.findWhere(originalAction.resources, {resourceId: resource.resourceId});
          }
          deferred.resolve(resources.selected[resource.resourceId]);
        });
      }
    } else {
      // un-select resource
      i = _.map(action.resources, function(r) { return r.resourceId; }).indexOf(resource.resourceId);
      if (i > -1) {
        // cache entered values
        resources.removed[resource.resourceId] = action.resources.splice(i, 1)[0];
      }

      deferred.resolve(resources.selected[resource.resourceId]);
    }
    $scope.$emit('xras:requestChanged', { key: 'resources' });
    return deferred.promise;
  };

  // get $scope.action currently selected resources;
  $q.all(_.map($scope.action.resources, function(r) {
    return processSelection(r, true);
  })).then(resourceAttributesForm);
});
