'use strict';
angular.module('xrasApp').controller('StartRequestCtrl', function ($scope, $routeParams, $location, Api, OpportunityUtil) {
  var opportunityId = $routeParams.opportunityId,
    requestId = $routeParams.requestId;

  var promise = Api.Opportunities.one(opportunityId).get();
  promise.then(function() {
    $scope.summaryInfo = OpportunityUtil.allocationTypeSummary($scope.opportunity);
    // probably doing this "wrong" but I can't figure out the bullshit angular way so here we are
    var type = $scope.opportunity.allocationType.toLowerCase().replace(/ /g, '_');
    $scope.requirements = $scope.requirements[type]['new'];

    $scope.reqsMap = {
        'title': 'Title',
        'abstract': 'Abstract',
        'principal_investigator': 'PI',
        'keywords': 'Keywords',
        'field_of_science': 'Field of Science',
        'documents': 'Documents',
        'publications': 'Publications',
        'resources': 'Resources',
        'end_date': 'End Date',
        'user_comment': 'User Comments',
    };
  });
  $scope.opportunity = promise.$object;

  $scope.createRequest = function(advanced) {
    var req = {
      opportunityId: opportunityId
    };
    var formUrl = '';
    if (requestId) {
          Api.Requests.one(requestId).get().then(
            function(resp) {
                // we have to manually set the opportunityId here because some renewals (XRAC)
                // are on *new* opportunities
                resp.opportunityId = opportunityId;
                Api.Renewal.create(resp).then(function(renewResp) {
                        $scope.renewalNumber = renewResp.renewalNumber;
                          formUrl = '/requests/' + renewResp.requestId + (advanced ? '/edit/advanced' : '/edit');
                          $location.path(formUrl);
                              }, function() {
                                window.alert('There was an error creating your Request! Please try again.');
                      });
                });
    } else {
      req.requestType = 'New';

    Api.Requests.post(req).then(function(resp) {
         formUrl = '/requests/' + resp.requestId + (advanced ? '/edit/advanced' : '/edit');
         $location.path(formUrl);
    }, function() {
      window.alert('There was an error creating your Request! Please try again.');
    });
    }
  };
});
