'use strict';
angular.module('xrasApp').controller('RequestFormAdvancedCtrl', function($scope, $routeParams, $location, $anchorScroll, $q, _, Api, Notify, RequestUtil) {

  $scope.request = {};

  // deferreds for all of our lookups
  $scope.loading = true;
  var deferred = {
    fos: $q.defer(),
    request: $q.defer()
  };

  // after everything is loaded, emit ready event
  var promises = _.map(deferred, function(d) { return d.promise; });
  $q.all(promises).then(function() {
    $scope.$broadcast('xras:requestReady', $scope.request);
    $scope.loading = false;
  });

  // Types
  $scope.types = {};

  // Fields of Science
  Api.Fos.getList().then(function(list) {
    $scope.fields = list;
    deferred.fos.resolve(list);
  });

  Api.Requests.one($routeParams.requestId).get()
  // check that request is editable
  .then(function(request) {
    if (request.requestStatus === 'Incomplete') {
      // pass to next thenable
      return request;
    } else {
      var confirmEdit = $q.defer();
      if (request.requestStatus === 'Submitted') {
        // prompt to withdraw submission
        if (window.confirm('This request has already been submitted! If you edit it now it will be withdrawn and you will have to resubmit the request.\n\nClick "OK" to continue.')) {
          // withdraw then pass to next thenable
          Api.Actions.withdraw(request, RequestUtil.getOriginalAction(request)).then(function() {
            confirmEdit.resolve(request);
          }, function(error) {
            confirmEdit.reject(error);
          });
        } else {
          // reject
          confirmEdit.reject(undefined);
        }
      } else {
        // disallow edits altogether
        confirmEdit.reject('This request is not editable.');
      }
      return confirmEdit.promise;
    }
  }, function(error) {
    if (error.status === 404) {
      Notify.message({
        body: 'The Request was not found!',
        type: 'danger'
      });
    } else {
      Notify.message({
        body: 'There was and error loading the Request. Please try again.',
        type: 'danger'
      });
    }
    $location.path('/');
  })
  // setup scope
  .then(function(request) {
    $scope.request = request;
    $scope.action = RequestUtil.getOriginalAction($scope.request);
    Api.Opportunities.one(request.opportunityId).get().then(function(opp) {
      $scope.opportunity = opp;
      deferred.request.resolve(request);
    });
  }, function(reason) {
    if (reason) {
      Notify.message({
        body: reason,
        type: 'danger'
      });
    }
    window.history.back();
  });

  // final Save handler
  $scope.submitRequest = function() {
    if ($scope.saving) {
      return;
    }

    Notify.clear('all');

    $scope.saving = true;
    $scope.request.save().then(
      function(request) {
        // validate request primary action
        $scope.request = request;
        $scope.action = RequestUtil.getOriginalAction(request);
        return Api.Actions.validate($scope.request, $scope.action);
      }, function(reason) {
        $scope.saving = false;
        if (reason.status === 400) {
          Notify.message({
            body: reason.statusText + ': ' + reason.data,
            type: 'danger'
          });
        } else {
          Notify.message({
            body: 'An unexpected error occurred while saving the Request. Please try again. If this problem persists, please contact the Help Desk.',
            type: 'danger'
          });
        }
        $anchorScroll();
      })
      .then(function(validation) {
        if (validation.validation === 'failed') {
          return $q.reject(validation);
        } else {
          // submit request
          return Api.Actions.submit($scope.request, $scope.action);
        }
      }, function() {
        $scope.saving = false;
        Notify.message({
          body: 'An unexpected error occurred while validating your Request. Please try again. If this error persists, please contact the Help Desk.',
          type: 'danger'
        });
        $anchorScroll();
      })
      .then(function(request) {
        // done! notify and redirect
        $scope.saving = false;
        Notify.message({
          body: 'Congratulations! Your Request has been submitted!',
          type: 'success'
        });
        $location.path('/requests/' + request.requestId);
      }, function(validationError) {
        $scope.saving = false;
        Notify.message({
          body: 'Request validation failed! Please fix the reported errors and try again.',
          type: 'danger'
        });
        _.each(validationError.errors, function(error) {
            Notify.message({
              body: error,
              type: 'danger'
            });
        });
        $anchorScroll();
      });
  };

  $scope.saveRequest = function() {
    if ($scope.saving) {
      return;
    }
    Notify.clear('all');
    $scope.saving = true;
    $scope.request.save().then(
      function(resp) {
        $scope.saving = false;
        Notify.message({
          body: 'Your Request has been saved! Please note, you still need to submit your request before it can be considered for review.',
          type: 'success'
        });
        $location.path('/requests/' + resp.requestId);
      }, function(reason) {
        $scope.saving = false;
        if (reason.status === 400) {
          Notify.message({
            body: reason.statusText + ': ' + reason.data,
            type: 'danger'
          });
        } else {
          Notify.message({
            body: 'An unexpected error occurred while saving the Request. Please try again. If this problem persists, please contact the Help Desk.',
            type: 'danger'
          });
        }
        $anchorScroll();
      }
    );
  };

  $scope.cancel = function() {
    Notify.clear('all');
    if ($scope.saving) {
      return;
    }
    if ($scope.requestForm.$dirty && !window.confirm('Wait! You have unsaved changes to this form! Are you sure you want to discard you changes? You can click "Save for Later" to save changes without submitting your request.\n\nClick "OK" to discard changes.')) {
      return;
    }
    $location.path('/requests/' + $scope.request.requestId);
  };

  $scope.guided = function() {
    Notify.clear('all');
    if ($scope.requestForm.$dirty) {
      // save changes first!
      $scope.saving = true;
      $scope.request.save().then(function() {
        $location.path('/requests/' + $scope.request.requestId + '/edit');
      }, function(reason) {
        $scope.saving = false;
        if (reason.status === 400) {
          Notify.message({
            body: reason.statusText + ': ' + reason.data,
            type: 'danger'
          });
        } else {
          Notify.message({
            body: 'An unexpected error occurred while saving the Request. Please try again. If this problem persists, please contact the Help Desk.',
            type: 'danger'
          });
        }
        $anchorScroll();
      });
    } else {
      $location.path('/requests/' + $scope.request.requestId + '/edit');
    }
  };

});
