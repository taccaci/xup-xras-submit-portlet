'use strict';
angular.module('xrasApp').controller('RequestFormInfoCtrl', function($scope, _) {

  $scope.$on('xras:requestReady', function() {
    if ($scope.opportunity.allocationType === 'Educational' || $scope.opportunity.allocationType === 'Startup' || $scope.opportunity.allocationType === 'XSEDE Campus SSO Integration') {
          if ($scope.action.allocationDates.length === 0) {
            $scope.action.allocationDates[0] = {
              allocationDateType: 'Requested',
              beginDate: '',
              endDate: ''
            };
          }
          $scope.allocationDate = $scope.action.allocationDates[0];
     } else {
    if (! $scope.opportunity.defaultAllocationAwardPeriod) {
      if ($scope.action.allocationDates.length === 0) {
        $scope.action.allocationDates[0] = {
          allocationDateType: 'Requested',
          beginDate: '',
          endDate: ''
        };
      }
      $scope.allocationDate = $scope.action.allocationDates[0];
    }
    }

    $scope.activeFos = $scope.fields.filter(function(field) {
        return field.isActive;
    });

    _.each($scope.activeFos, function(field) {
        if (field.fosTypeParentId !== null) {
            if (field.isSelectable) {
                field.displayName = '  -- ' + field.fosName;
            } else {
                field.displayName = ' - ' + field.fosName;
            }
        } else {
            field.displayName = field.fosName;
        }
    });
  });

  $scope.dateChange = function() {
    // clearing out the begin and end date on a click
    $scope.allocationDate.beginDate = null;
    $scope.allocationDate.endDate = null;
    $scope.$emit('xras:requestChanged', { key: 'date' });
  };

  $scope.datepickerOpts = {
    format: 'yyyy-mm-dd',
    autoclose: true,
    startDate: '+1m',
    placeholder: 'YYYY-MM-DD'
  };

    $scope.startEndDateOpts = {
      format: 'yyyy-mm-dd',
      autoclose: true,
      startDate: 'today',
      placeholder: 'YYYY-MM-DD'
    };

  $scope.addFos = function() {
    $scope.request.fos = $scope.request.fos || []; // first fos!
    var fos = {};
    if ($scope.request.fos.length === 0) {
      fos.isPrimary = true;
    }
    $scope.request.fos.push(fos);
  };

  $scope.selectPrimaryFos = function(index) {
    _.each($scope.request.fos, function(field, i) {
      field.isPrimary = i === index;
    });
  };

  $scope.removeFos = function(index) {
    $scope.request.fos.splice(index, 1);
    $scope.$emit('xras:requestChanged', { key: 'fos' });
  };
});
