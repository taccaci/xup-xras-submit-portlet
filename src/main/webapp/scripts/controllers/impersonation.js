'use strict';
angular.module('xrasApp').controller('ImpersonationCtrl', function($scope, $rootScope, $route, $location, _, Api) {

  Api.Users.one().one('impersonate').get().then(function(resp) {
    $scope.impersonating = resp;
  });

  $scope.canImpersonate = function() {
    var roles = $rootScope.user && $rootScope.user.roles || [];
    return _.contains(roles, 'Allocations Admin') || _.contains(roles, 'Administrator');
  };

  $scope.stopImpersonate = function(username) {
    Api.Users.one().one('impersonate', username).remove().then(function(resp) {
      $scope.impersonating = null;
      if (resp) {
        if ($location.path() === '/') {
          $route.reload();
        } else {
          $location.path('/');
        }
      }
    });
  };

  $scope.startImpersonate = function(username) {
    Api.Users.one().one('impersonate', username).post().then(function(resp) {
      if (resp) {
        $scope.impersonating = username;
        if ($location.path() === '/') {
          $route.reload();
        } else {
          $location.path('/');
        }
      }
    });
  };
});
