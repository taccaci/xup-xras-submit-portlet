/**
 * file: controllers/main.js
 * Controller for the default view. Displays currently available Opportunities
 * and Requests.
 */

'use strict';
angular.module('xrasApp').controller('MainCtrl', function ($scope, _, Api, OpportunityUtil, RequestUtil, Notify) {
  $scope.loading = {
    opportunities: true,
    requests: true
  };

  Api.Opportunities.getList().then(function(list) {
    $scope.loading.opportunities = false;
    $scope.opportunities = list;
  }, function() {
    $scope.loading.opportunities = false;
    $scope.opportunities = [];
    Notify.message({
      type: 'danger',
      body: 'There was an error loading Opportunities!'
    });
  });

  Api.Requests.getList().then(function(list) {
    $scope.loading.requests = false;
    $scope.requests = list;
    $scope.statuses = _.chain(list).pluck('requestStatus').uniq().sortBy(RequestUtil.requestStatusSort).value();
    $scope.requestFilter = $scope.statuses[0];

    $scope.requestOpportunities = {};
    _.chain(list).pluck('opportunityId').each(function(oppId) {
      Api.Opportunities.one(oppId).get().then(function(opportunity) {
        $scope.requestOpportunities[oppId] = opportunity;
      });
    });

    var incompleteActions = [];

    for (var i = 0; i < list.length; i++) {
        var r = list[i];
        if (r.requestStatus === 'Approved') {
        for (var j = 0; j < r.actions.length; j++) {
            var a = r.actions[j];
            if (a.actionStatus === 'Incomplete' && 'requestNumber' in r && !a.isDeleted) {
               incompleteActions.push(r.requestId);
            }
        }
        }
    }

    $scope.incompleteActions = _.uniq(incompleteActions);

  }, function() {
    $scope.loading.requests = false;
    $scope.requests = [];
    Notify.message({
      type: 'danger',
      body: 'There was an error loading Requests!'
    });
  });

  Api.Announcements.getList().then(function(list) {
        $scope.announcements = list;
  });

  $scope.opportunitySort = OpportunityUtil.defaultSort;

  $scope.requestRoleSort = RequestUtil.requestRoleSort;

  $scope.allowRequestOp = RequestUtil.isAllowedOperation;

  $scope.filterRequestList = function(status) {
    $scope.requestFilter = status;
  };

});
