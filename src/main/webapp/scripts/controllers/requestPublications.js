'use strict';
angular.module('xrasApp').controller('RequestFormPublicationsCtrl', function($scope, $cacheFactory, _, Notify, Api) {

  $scope.$on('xras:requestReady', function() {
    $scope.updateAvailablePubs();
  });

  $scope.updateAvailablePubs = function() {
    $scope.refreshing = true;
    $scope.availablePublications = [];
    var usernames = _.chain($scope.request.roles).pluck('person').pluck('username').value().join(',');
    var requestNumber = $scope.request.requestNumber;

    // force refresh always
    $cacheFactory.get('xrasApi').remove('/delegate/xras/publications/username/'+usernames);

    if (requestNumber !== null) {
        $cacheFactory.get('xrasApi').remove('/delegate/xras/publications/requestNumber/TG-'+requestNumber);
    }

    Api.Publications.one().customGETLIST('username/' + usernames).then(function(list) {
      $scope.refreshing = false;
      $scope.availablePublications = list;

          if (requestNumber !== null) {
            Api.Publications.one().customGETLIST('requestNumber/TG-' + requestNumber).then(function(list) {
              $scope.availablePublications.push(list);
            });
           }

    });
  };

    $scope.doiSearch = {};
    var foundDois = [];
    $scope.searchByDoi = function() {
    $scope.doiSearch.doiSearchError = false;
      Api.Publications.one().post('doi', $scope.doiSearch.doi).then(function(publication) {
           if (publication ) {
            if (foundDois.indexOf(publication.doi) !== -1) {
                // it is in the request list already so return an error
                $scope.doiSearch.doiSearchError = true;
                $scope.doiSearch.doiSearchErrorText = 'A publication with this DOI has already been added. Click the \'Add to Request\' button next to it to add it to your request.';
            } else {
                $scope.doiSearch.doiSearchError = false;
                foundDois.push(publication.doi);
                $scope.availablePublications.push(publication);
            }
          } else {
            $scope.doiSearch.doiSearchError = true;
            $scope.doiSearch.doiSearchErrorText = 'We could not find a publication matching this DOI please check your formatting and verify you are using the correct DOI.';
          }
      });
    };

  $scope.toggleAvailablePubs = function(show) {
    $scope.editing = show;
  };

  $scope.filterAddedPubs = function(publication) {
    return ! _.find($scope.request.publications, function(rp) {
      return rp.publication.id === publication.id;
    });
  };

  $scope.add = function(index, pub) {
    $scope.request.publications = $scope.request.publications || [];
    $scope.request.publications.push({
      publication: pub
    });
    $scope.$emit('xras:requestChanged', { key: 'publications' });
  };

  $scope.remove = function($index, pub) {
    if (window.confirm('Are you sure you want to remove the publication "' + pub.publication.title +'"?')) {
      $scope.request.publications.splice($index, 1);
      $scope.$emit('xras:requestChanged', { key: 'publications' });
    }
  };
});
