package edu.utexas.tacc.xras.api;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.api.exception.AuthException;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.Announcement;
import edu.utexas.tacc.xras.service.AnnouncementService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value="/xras/announcements", method=RequestMethod.GET)
public class AnnouncementsApi extends BaseApiController {

    @Autowired
    AnnouncementService announcementService;

    @RequestMapping
    public @ResponseBody
    List<Announcement> announcements(HttpServletRequest request) throws AuthException, ServiceException {
        XrasSession session = getXrasSession(request);
        List<Announcement> announcements = announcementService.list(session);
        List<Announcement> currentAnnouncements = new ArrayList<>();

        for (Announcement a : announcements) {
            if (a.getBeginDate().isBeforeNow() && a.getEndDate().isAfterNow()) {
                currentAnnouncements.add(a);
            }
        }

        return currentAnnouncements;
    }

}
