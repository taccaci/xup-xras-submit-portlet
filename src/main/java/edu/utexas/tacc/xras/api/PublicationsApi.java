/**
 *
 */
package edu.utexas.tacc.xras.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import de.undercouch.citeproc.csl.CSLType;
import edu.utexas.tacc.hibernate.bean.*;
import edu.utexas.tacc.hibernate.dao.PublicationTypeDao;
import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Request;
import org.apache.http.message.BasicHeader;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import edu.utexas.tacc.exceptions.QueryException;
import edu.utexas.tacc.hibernate.dao.PublicationDao;
import edu.utexas.tacc.xras.ServiceException;

/**
 * @author mrhanlon
 */
@Controller
@RequestMapping(value = "/xras/publications")
public class PublicationsApi extends BaseApiController {

    private static final Logger logger = Logger.getLogger(PublicationsApi.class);

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Look up publications for all the people on a request.
     *
     * @param request
     * @return
     * @throws ServiceException
     */
    @Transactional
    @RequestMapping(value = "/username/{usernames}", method = RequestMethod.GET)
    public @ResponseBody
    List<Publication> getXcdbPublications(HttpServletRequest request, @PathVariable String usernames) throws ServiceException {
        logger.debug("calling getXcdbPublications(" + usernames + ")");
        List<Publication> pubs;
        try {
            Session session = sessionFactory.getCurrentSession();
            PublicationDao dao = new PublicationDao(session);
            pubs = dao.getPublicationsByUsernameCreatorsList(usernames.split(","));
        } catch (QueryException e) {
            logger.error(e);
            throw new ServiceException(e);
        }

        // load lazy object before serialization
        for (Publication p : pubs) {
            Hibernate.initialize(p.getAuthors());
            Hibernate.initialize(p.getData());
            Hibernate.initialize(p.getProjects());
        }

        return pubs;
    }

    @Transactional
    @RequestMapping(value = "/projectNumber/{projectNumber}", method = RequestMethod.GET)
    public @ResponseBody
    List<Publication> getXcdbProjectPublications(HttpServletRequest request, @PathVariable String projectNumber) throws ServiceException {
        logger.debug("calling getXcdbPublications(" + projectNumber + ")");
        List<Publication> pubs;
        try {
            Session session = sessionFactory.getCurrentSession();
            PublicationDao dao = new PublicationDao(session);
            pubs = dao.getPublicationsByProject(projectNumber);
        } catch (QueryException e) {
            logger.error(e);
            throw new ServiceException(e);
        }

        // load lazy object before serialization
        for (Publication p : pubs) {
            Hibernate.initialize(p.getAuthors());
            Hibernate.initialize(p.getData());
            Hibernate.initialize(p.getProjects());
        }

        return pubs;
    }


    @Transactional
    @RequestMapping(value = "/doi", method = RequestMethod.POST)
    public @ResponseBody
    Publication getPublicationByDoi(HttpServletRequest request, @RequestBody String doi) throws ServiceException {
        String p = "";
        Publication newPub = new Publication();
        Session session = sessionFactory.getCurrentSession();
        PublicationDao pubDao = new PublicationDao(session);
        Header header = new BasicHeader("Accept", "applications/rdf+xml;q=0.5,application/vnd.citationstyles.csl+json;q=1.0");
        String uri = "http://dx.doi.org/" + doi;
        try {
            newPub = pubDao.getPublicationByDOI(doi);
            if (newPub != null) {
                logger.info("The pub with doi " + doi + " exists in our db already, loading into request.");
                // exists in the db already, do the lazy object load thing
                Hibernate.initialize(newPub.getAuthors());
                Hibernate.initialize(newPub.getData());
                Hibernate.initialize(newPub.getProjects());
                return newPub;
            } else {
                Request req = Request.Get(uri);
                req.addHeader(header);
                p = req.execute().returnContent().asString();
                if (!p.equals("")) {
                    logger.info("Publication with DOI " + doi + " was loaded from the dx.doi api. creating and saving new publication.");
                    String username = PortalUtil.getUser(request).getScreenName();
                    // stuff into pub model, save it, return it
                    newPub = map(p, session);
                    newPub.setStatus("published");
                    newPub.setCreated(new DateTime());
                    newPub.setCreatedBy(username);
                    newPub.setModified(newPub.getCreated());
                    newPub.setModifiedBy(newPub.getCreatedBy());
                    pubDao.save(newPub);
                }
                return newPub;
            }
        } catch (ClientProtocolException e) {
            logger.error(e);
        } catch (IOException e) {
            logger.error(e);
        } catch (QueryException e) {
            logger.error(e);
        } catch (PortalException e) {
            logger.error(e);
        } catch (SystemException e) {
            logger.error(e);
        }

        return newPub;
    }

    /**
     * stolen from publications-submit-portlet...
     * this is how it hydrates a Publications object
     *
     * @return
     */

    private static Publication map(String publicationJson, Session session) {
        ObjectMapper mapper = new ObjectMapper();
        Publication bean = new Publication();

        try {
            JsonNode record = mapper.readTree(publicationJson);
            bean.setTitle(record.get("title").asText());
            bean.setDoi(record.get("DOI").asText());

            try {
                JsonNode issued = record.get("issued");
                if (issued.has("date-parts")) {
                    issued = issued.get("date-parts").get(0);
                    int year = record.get("issued").get("date-parts").get(0).get(0).asInt();
                    bean.setYear(year);
                    if (issued.size() > 1) {
                        int month = record.get("issued").get("date-parts").get(0).get(1).asInt();
                        bean.setMonth(month);
                    }
                } else if (issued.has("raw")) {
                    bean.setYear(issued.get("raw").asInt());
                }
            } catch (Exception e) {
                logger.warn("Unable to parse issued date", e);
            }

            int authorCounter = 0;
            for (JsonNode authorNode : record.get("author")) {
                if (authorCounter >= 50) {
                    break;
                } else {
                    Author author = new Author();
                    if (authorNode.has("given") || authorNode.has("family")) {
                        if (authorNode.has("given")) {
                            author.setFirstName(authorNode.get("given").asText());
                        }
                        if (authorNode.has("family")) {
                            author.setLastName(authorNode.get("family").asText());
                        }
                    } else if (authorNode.has("literal")) {
                        String name = authorNode.get("literal").asText();
                        String[] parts = name.split(" ");
                        if (parts.length == 3) {
                            author.setFirstName(parts[0]);
                            author.setMiddleName(parts[1]);
                            author.setLastName(parts[2]);
                        } else if (parts.length == 2) {
                            author.setFirstName(parts[0]);
                            author.setLastName(parts[1]);
                        } else {
                            author.setLastName(name);
                        }
                    }
                    bean.addAuthor(new PublicationAuthor(author));
                }
                authorCounter++;
            }
            CSLType cslType = getCSLType(record);

            PublicationTypeDao pubTypeDao = new PublicationTypeDao(session);

            try {
                PublicationType pubType = pubTypeDao.getByCSLType(cslType.toString());

                bean.setType(pubType);
                for (PublicationField field : pubType.getFields()) {
                    PublicationData fieldData = new PublicationData();
                    fieldData.setField(field);
                    if (field.getCslFieldName() != null && record.get(field.getCslFieldName()) != null) {
                        fieldData.setValue(record.get(field.getCslFieldName()).asText());
                    }
                    bean.addData(fieldData);
                }
            } catch (QueryException e) {
                logger.error(e);
            }

        } catch (IOException e) {
            logger.error(e);
        }

        return bean;
    }

    private static CSLType getCSLType(JsonNode record) {
        String cslTypeName;
        if (record.get("type") != null) {
            cslTypeName = record.get("type").asText();
        } else {
            cslTypeName = "journal-article";
        }
        CSLType cslType;
        try {
            cslType = CSLType.fromString(cslTypeName);
        } catch (IllegalArgumentException e) {
            if (cslTypeName.equals("book-chapter")) {
                cslType = CSLType.CHAPTER;
            } else if (cslTypeName.equals("conference-paper") || cslTypeName.equals("proceedings-article")) {
                cslType = CSLType.PAPER_CONFERENCE;
            } else {
                cslType = CSLType.ARTICLE_JOURNAL;
            }
        }

        return cslType;
    }
}
