/**
 * 
 */
package edu.utexas.tacc.xras.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.service.PersonService;

/**
 * @author mrhanlon
 *
 */
@Controller
@RequestMapping(value="/xras/users")
public class UsersApi extends BaseApiController {

  @Autowired
  private PersonService personService;
  
  @RequestMapping(method=RequestMethod.GET)
  public @ResponseBody Map<String,Object> user(HttpServletRequest request) throws ServiceException {
    try {
      User user = PortalUtil.getUser(request);
      Map<String,Object> response = new HashMap<String,Object>();
      response.put("firstName", user.getFirstName());
      response.put("lastName", user.getLastName());
      response.put("username", user.getScreenName());

      List<String> roleNames = new ArrayList<String>();
      List<Role> roles = RoleLocalServiceUtil.getUserRoles(user.getUserId());
      for (Role r : roles) {
        roleNames.add(r.getName());
      }
      
      response.put("roles", roleNames);
      return response;
    } catch (Exception e) {
      throw new ServiceException(e);
    }
  }
  
  @RequestMapping(value="/impersonate", method=RequestMethod.GET)
  public @ResponseBody String getImpersonate(HttpServletRequest request) {
    String impersonating = (String) request.getSession().getAttribute("impersonateUser");
    return impersonating == null ? "" : impersonating;
  }
  
  @RequestMapping(value="/impersonate/{username}", method=RequestMethod.POST)
  public @ResponseBody Boolean doImpersonate(HttpServletRequest request, @PathVariable String username) throws ServiceException {
    try {
      long companyId = PortalUtil.getCompanyId(request);
      long userId = PortalUtil.getUserId(request);
      if (
        UserLocalServiceUtil.hasRoleUser(companyId, "Administrator", userId, true) ||
        UserLocalServiceUtil.hasRoleUser(companyId, "Allocations Admin", userId, true)
       ) {
        request.getSession().setAttribute("impersonateUser", username);          
        return true;
      } else {
        return false;
      }
    } catch (Exception e) {
      throw new ServiceException(e);
    }
  }
  
  @RequestMapping(value="/impersonate/{username}", method=RequestMethod.DELETE)
  public @ResponseBody Boolean cancelImpersonate(HttpServletRequest request, @PathVariable String username) throws ServiceException {
    try {
      long companyId = PortalUtil.getCompanyId(request);
      long userId = PortalUtil.getUserId(request);
      if (
        UserLocalServiceUtil.hasRoleUser(companyId, "Administrator", userId, true) ||
        UserLocalServiceUtil.hasRoleUser(companyId, "Allocations Admin", userId, true)
       ) {
        request.getSession().removeAttribute("impersonateUser");
        return true;
      } else {
        return false;
      }
    } catch (Exception e) {
      throw new ServiceException(e);
    }
  }

}
