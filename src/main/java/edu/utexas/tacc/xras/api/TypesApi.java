/**
 * 
 */
package edu.utexas.tacc.xras.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import edu.utexas.tacc.xras.model.type.*;
import edu.utexas.tacc.xras.service.type.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.api.exception.AuthException;
import edu.utexas.tacc.xras.http.session.XrasSession;

/**
 * @author mrhanlon
 *
 */
@Controller
@RequestMapping(value="/xras/types", method=RequestMethod.GET)
public class TypesApi extends BaseApiController {

  @Autowired
  private AttributeSetRelationTypeService attrSetRelService;

  @Autowired
  private UnitTypeService unitService;

  @Autowired
  private DocumentTypeService docTypeService;
  
  @Autowired
  private RoleTypeService roleService;
  
  @Autowired
  private FieldOfScienceTypeService fosService;

  @Autowired
  private ActionTypeService actionTypeService;

  @RequestMapping(value="/units")
  public @ResponseBody List<UnitType> units(HttpServletRequest request) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);
    return unitService.list(session);
  }

  @RequestMapping(value="/attribute_set_relations")
  public @ResponseBody Map<Integer, AttributeSetRelationType> attributeSetRelations(HttpServletRequest request) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);
    List<AttributeSetRelationType> list = attrSetRelService.list(session);
    Map<Integer, AttributeSetRelationType> map = new HashMap<Integer, AttributeSetRelationType>();
    for (AttributeSetRelationType type : list) {
      map.put(type.getId(), type);
    }
    return map;
  }

  @RequestMapping(value="/documents")
  public @ResponseBody List<DocumentType> documents(HttpServletRequest request) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);
    return docTypeService.list(session);
  }

  // TODO new function that does the filtering? this probably won't necessarily solve the issue though

  @RequestMapping(value="/roles")
  public @ResponseBody List<RoleType> roles(HttpServletRequest request) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);
    return roleService.list(session);
  }

  @RequestMapping("/fos")
  public @ResponseBody List<FieldOfScienceType> fos(HttpServletRequest request) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);
    return fosService.list(session);
  }

  @RequestMapping(value="/fos", params="map=true")
  public @ResponseBody Map<Integer, FieldOfScienceType> fosMap(HttpServletRequest request) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);

    List<FieldOfScienceType> fosList = fosService.list(session);
    Map<Integer, FieldOfScienceType> fosMap = new HashMap<Integer, FieldOfScienceType>();
    for (FieldOfScienceType fos : fosList) {
      fosMap.put(fos.getId(), fos);
    }
    return fosMap;
  }

  @RequestMapping(value="/actionType/{actionType}", method=RequestMethod.GET)
  public @ResponseBody
  int request(HttpServletRequest request, @PathVariable String actionType) throws ServiceException, AuthException {
    XrasSession session = getXrasSession(request);
    int actionTypeId = -1;

    List<ActionType> actionTypes = actionTypeService.list(session);

    for (ActionType a : actionTypes) {
      if (a.getActionType().equals(actionType)) {
        actionTypeId = a.getId();
      }
    }
    return actionTypeId;
  }
}
