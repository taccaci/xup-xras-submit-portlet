package edu.utexas.tacc.xras.api;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.api.exception.AuthException;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.RequiredFields;
import edu.utexas.tacc.xras.service.AbstractPortletService;
import edu.utexas.tacc.xras.service.RequiredFieldsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.util.List;

@Controller
@RequestMapping(value = "/xras/requiredFields")
public class RequiredFieldsApi extends BaseApiController {

    @Autowired
    private AbstractPortletService portletService;

    @Autowired
    private RequiredFieldsService requiredFieldsService;

    @RequestMapping(value = "{allocationTypeId}/{actionTypeId}", method = RequestMethod.GET)
    public
    @ResponseBody
    RequiredFields get(HttpServletRequest request, @PathVariable int allocationTypeId, @PathVariable int actionTypeId) throws AuthException, ServiceException {
        XrasSession session = getXrasSession(request);
        DataSource ds = portletService.getDataSource();

        RequiredFields fields = new RequiredFields();

        return requiredFieldsService.read(session, fields, allocationTypeId, actionTypeId);

    }
}
