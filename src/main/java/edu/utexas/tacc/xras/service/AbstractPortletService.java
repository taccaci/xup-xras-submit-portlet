package edu.utexas.tacc.xras.service;

/**
 * Created by carrie on 12/21/16.
 */
import javax.naming.NamingException;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.stereotype.Service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

import edu.utexas.tacc.bean.PortalPerson;
import edu.utexas.tacc.portlets.AbstractPortlet;

@Service(value="abstractPortletService")
public class AbstractPortletService extends AbstractPortlet {

    public PortalPerson getPerson(PortletRequest request) {

        PortalPerson person = null;

        try {
            person = getCurrentPerson(request);
        } catch (PortalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SystemException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return person;
    }

    public DataSource getDataSource() {

        DataSource ds = null;

        try {
            ds = getPortalDataSource();
        } catch (NamingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return ds;
    }

    public boolean userHasRole(PortletRequest request, String role) throws PortalException, SystemException {
        return userHasRole(PortalUtil.getHttpServletRequest(request), role);
    }

    public boolean userHasRole(HttpServletRequest request, String role) throws PortalException, SystemException {
        com.liferay.portal.model.User user = PortalUtil.getUser(request);

        // causing null pointer exceptions if the user isn't logged in
        if (user != null) {
            return UserLocalServiceUtil.hasRoleUser(user.getCompanyId(), role, user.getUserId(), true);
        } else {
            return false;
        }
    }
}
